import { Component, OnInit } from '@angular/core';

import {ApiService} from '../../servicios/api/api.service';
import {Router} from '@angular/router'; 
import { HttpClient, HttpHeaders} from '@angular/common/http';


 import { ListaheroesI } from '../../interfaces/listaheroes.interface';
 import { HeroeI} from '../../interfaces/heroe.interface';
 import {PowerstatsI} from '../../interfaces/powerstats.interface'



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  heroes:ListaheroesI[];

  apiResponse:string='';
  

  constructor(private api:ApiService, private router:Router, private http:HttpClient) { 
    this.heroes=[];
  }

  ngOnInit(): void {
    // console.log("metodo oninit")
    // this.api.getheroe(1).subscribe(data =>{
    //   console.log(data);
    }

  //  prueba(){
  //    this.api.getheroe().subscribe(data=>{
  //     console.log(data);})
     
  //  }


  // prueba(){
  //   this.api.getheroe().subscribe(data=>{
  //     this.api.setToken(localStorage.getItem('token'));
  //      console.log(data);
  //     //  this.api.setToken('token');
  //   },
  //   error=>{
  //     console.log(error);
  //   })
    
  //   }

    prueba() {
      this.api.getheroe().subscribe(data => {
        console.log(data);
      });
}

}
