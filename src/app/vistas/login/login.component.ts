import { Component, OnInit } from '@angular/core';
// import { HttpClient , HttpHeaders, HttpRequest, HttpClientModule} from '@angular/common/http';

import {FormGroup, FormControl, Validators, FormBuilder} from '@angular/forms';
import { ApiService } from 'src/app/servicios/api/api.service';
import { LoginI } from 'src/app/interfaces/login.interface';
import { ResponseI } from 'src/app/interfaces/response.interface';

import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', Validators.required)
  })

  constructor(private api:ApiService, private router:Router) { }


  ngOnInit(): void {
  }

  onLogin(form:LoginI){
    const tokenn = localStorage.getItem('token');
    this.api.loginEmail(form).subscribe(data=>{
       console.log(data);
      let dataResponse:ResponseI=data;
    //  if (dataResponse.status=="ok"){
     localStorage.setItem('token', dataResponse.token);
    //  this.api.setToken('token');
        this.router.navigate(['home']);
    //  }
     console.log(form);
     console.log(this.loginForm.value); 
     console.log("hello mi token");
     console.log(localStorage.getItem('token'));

     },
     error=>{
      console.log(error);
     }
     )

  

  }

  // })
  // if (this.loginForm.get.===""){
  //   console.log("El campo no puede estar vacio");
  // }
  
}
