import { Injectable } from '@angular/core';
import {LoginI} from 'src/app/interfaces/login.interface';
import {ResponseI} from 'src/app/interfaces/response.interface';
import {HttpClient , HttpHeaders, HttpRequest, HttpClientModule} from '@angular/common/http';
import { HttpInterceptor, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
// import { catchError, retry } from 'rxjs/operators';

import { ListaheroesI } from 'src/app/interfaces/listaheroes.interface';
import {HeroeI}from '../../interfaces/heroe.interface';
import {PowerstatsI} from '../../interfaces/powerstats.interface';
import { CookieService } from "ngx-cookie-service";

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  urlLogin:string="http://challenge-react.alkemy.org/";
  url:string="https://superheroapi.com/api/959251288313451/13/";

  constructor(private http:HttpClient, private cookies: CookieService) {
    
    // this.headers.append("Content-Type","application/json");
    // this.headers.append("authorization","Bearer"+ localStorage.getItem("token"));
   }

  //  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
  //   const token = localStorage.getItem('token');
  //   let request = req;
  //   if (token) {
  //     request = req.clone({
  //       setHeaders: {
  //         authorization: `Bearer ${ token }`
  //       }
  //     });
  //   }
  //   return next.handle(request);
  // }

 loginEmail(form:LoginI):Observable<ResponseI>{
   let direccion = this.urlLogin;
   return this.http.post<ResponseI>(direccion,form);
 }
 


// prueba(){
//   const token = this.getToken();
// }

// setToken(token: any) {
//   this.cookies.set("token", token);
// }

// getToken() {
//   return this.cookies.get("token");
// }

// getheroe() {
//   return this.http.get("https://superheroapi.com/api/959251288313451/13/");
// }
// getheroes(page:number):Observable<ListaheroesI[]>{
// let direccion=this.url,
// return this.http.get<ListaheroesI[]>(direccion);
// }

getheroe():Observable<PowerstatsI>{
  let direccion = this.url;
  return this.http.get<PowerstatsI>(direccion,);
}


  // getheroe():Observable<any>{
  //   let url = this.url;
  //   let tkn = localStorage.getItem("token");
  //   console.log("tkn / .." + tkn);
  //   const headers = new HttpHeaders();
  //   if (tkn != null){
  //       headers.set('Authorization', tkn).set('content-type','application/json; charset=UTF-8').set('Access-Control-Allow-Origin', '*').set('Accept','*/*');         
  //       console.log('headers:=>>>'+headers.getAll);
  //   }

  //   return this.http.get<any[]>(url,{ headers: headers })
  // }



// getheroe():Observable<any>{
//   console.log(localStorage.getItem('token'));
//    let direccion = this.url;
//    const headers = { 'Authorization': 'Bearer my-token',   'My-Custom-Header': 'foobar' }
//    this.http.get<any>(this.url, {headers}).subscribe(data=>{
//   return this
//     })
//   return this.http.get<any>(direccion);
//   }

}
