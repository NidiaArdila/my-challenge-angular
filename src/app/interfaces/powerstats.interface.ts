export interface PowerstatsI{
    intelligence: number, 
    strength:number,
    speed: number,
    durability: number,
    power: number,
    combat:number
}